# cpp-grpc-protobuf-cmake

# Intro
Docker container build for grpc & protobuf with cmake

# How to get it:
```
docker pull stulluk/cpp-grpc-protobuf-cmake
```

# Purpose
Just a playground to build c++ applications in a docker container with all required dependencies of grpc and protoc & cmake & compilers !

# Example Usage of Container
- Lets build simple helloworld grpc c++ example:
```
git clone -b v1.34.0 https://github.com/grpc/grpc   # No need submodules since we have all dependencies in the container. But this will download 300M!
cd grpc
docker run --rm -it -v $(pwd):/media/ stulluk/cpp-grpc-protobuf-cmake /bin/bash  # This will let you enter container's shell
cd /media/examples/cpp/helloworld/
cmake -B build .
cd build && make -j8
```

This will give you many binaries, such as greeter_server and greeter_client ( As well as their async versions !!)

exit from container. Open two terminals on your PC. In one terminal, run greeter_server first. And then, in other terminal, run greeter_client.

# Content

Following tools are included:
- g++ (amd64) 8.3.0
- g++ (aarch64) 8.3.0
- cmake 3.13.4
- grpc (branch: v1.34.0)
- protoc (3.13.0)
- build-essential
- autoconf
- libtool
- pkg-config
- file
- bash-completion
- vim