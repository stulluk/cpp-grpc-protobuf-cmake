FROM debian:buster-slim

RUN apt -y update && apt -y upgrade

RUN apt -y install build-essential cmake g++-aarch64-linux-gnu file bash-completion vim autoconf libtool pkg-config git

RUN echo "alias ll='ls -lah'" >> /root/.bashrc

RUN  /bin/bash -c 'export MY_INSTALL_DIR=/usr && \
	mkdir -p $MY_INSTALL_DIR && \
	export PATH="$PATH:$MY_INSTALL_DIR/bin" && \
	cd /opt/ && \
     	git clone --recurse-submodules -b v1.34.0 https://github.com/grpc/grpc && \
     	cd grpc && \
     	mkdir -p cmake/build && \
     	pushd cmake/build && \
     	cmake -DgRPC_INSTALL=ON \
	-DgRPC_BUILD_TESTS=OFF \
	-DCMAKE_INSTALL_PREFIX=$MY_INSTALL_DIR \
	../..   && \
     	make -j8 && \
     	make install && \
     	popd && \
	rm -rf /opt/grpc'

CMD "echo waww"
        
